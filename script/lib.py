#!/usr/local/bin/python3
#
# --------------------------------------------------------------------------------------------------
#
#   Purpose:
#       Script of re-usable code
#
# --------------------------------------------------------------------------------------------------

# imports
import sys
import os
import configparser
import os.path
import datetime
import uuid
import getpass
import subprocess
import argparse
from configparser import ExtendedInterpolation
from colorama import Fore, Back, Style, init
import fnmatch
from operator import itemgetter
import gzip
import shutil
import time
import getpass

# add 'pipeline/script/util' to system path
sys.path.append(os.getcwd() + '/util')

# path to my project
MY_REPO_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
# add 'ad-server/script/lib' to system path
#sys.path.append(MY_REPO_DIR + os.sep + 'script' + os.sep + 'lib')

# path to 'automated-tests' project dir (parent of MY_REPO_DIR)
AT_PROJECT_DIR = os.path.dirname(MY_REPO_DIR)

# add to 'automated-tests/common/lib' to system path
sys.path.append(AT_PROJECT_DIR + os.sep + 'common' + os.sep + 'lib')

print('--------------------------------------------------------------------------------')
print('MY_REPO_DIR: ' + MY_REPO_DIR)
print('AT_PROJECT_DIR: ' + AT_PROJECT_DIR)

# common project references
import google_sheets_api
import google_drive_api

# reset colorama for every print
init(autoreset=True)

# script version
script_version = '5.0'

hive_user = getpass.getuser()

# --------------------------------------
#   debug flags
# --------------------------------------

# turn verbose debugging on/off
debug = False
#debug = True

# turn upload on/off
do_upload = True
#do_upload = False

# turn save state on/off
do_save_state = True
#do_save_state = False

# data format
do_s3_format = True     # upload to s3
do_s3_format = False    # upload to adserver

# batch data
do_batches = True       # observer batches
# do_batches = False      # ignore batches

# test workbook
do_test_workbook = False    # don't use the test workbook
# do_test_workbook = True     # use the test workbook

# log aws upload details
do_print_aws = False
do_print_aws = True


# --------------------------------------


# -----------------------------------------------------------
def parse_args():
    global script_version
    global args

    # parse script arguments
    parser = argparse.ArgumentParser(description='Process arguments.')
    parser.add_argument('--test', '-t', required=True,
                        help='the NAME of the test')
    parser.add_argument('--testFileId', '-i', required=True,
                        help='the drive file id of the test')
    parser.add_argument('--config', '-c', required=True,
                        help='the Configuration to use')
    parser.add_argument('--version', '-v', action='version', version='%(prog)s ' + script_version)
    args = parser.parse_args()

    if debug:
        print('[DEBUG] script version:  ' + script_version)
        print('[DEBUG] args.test:       ' + args.test)
        print('[DEBUG] args.testFileId: ' + args.testFileId)
        print('[DEBUG] args.config:     ' + args.config)

# -----------------------------------------------------------
def init_globals():
    # declare globals
    global data_dir              # where to write generated data
    global timestamp_format     # transaction time format
    global field_length         # number of fields in a cookie log transaction
    global expected_col_count   # number of data columns in worksheet
    global row_num              # row number of worksheet
    global test_num             # test number
    global test_row             # logical test row
    global ua_suffix            # user agent suffix
    global test_time_stamp      # timestamp of the run
    global adserver_cookie_log  # current cookie log file on ad server
    global cookielog_time       # timestamp of cookie log file (yyyy-mm-dd-hh-mm)
    global hr_grain_format      # hour_id format (yyyy-mm-dd-hh)
    global s3_upload_dir        # s3 bucket to upload cookie log files to
    global do_s3_format         # use s3 upload or adserver
    global batch_count          # number of batches
    global expected_row_count   # number of total rows generated
    global failed_trans_rows    # rows of failed transactions
    global hive_server          # server that hive runs on
    global app_version          # 'version' of the pipeline app
    global app_deploytime       # 'deploy_time' of the pipeline app
    global app_deployuser       # 'deploy_user' of the pipeline app
    global gd_hive_file_id      # id of 'state' file on google drive
    global gd_state_file_id     # id of 'hive' file on google drive
    global gd_hive_folder_id    # id of the folder for 'hive'
    global gd_results_folder_id # id of the folder for 'results'

    # google drive file ids
    gd_hive_file_id = "0B3F1b7L5Cy3TSVMyRFBDS21PNGc"
    gd_state_file_id = "0B3F1b7L5Cy3TelVTVFBxSFVTLWM"
    gd_hive_folder_id = "0B3F1b7L5Cy3TcVEzYjVZZHl3X1E"
    gd_results_folder_id = '0B3F1b7L5Cy3TemMtRkFxSVFXdXc'

    # project directories
    parent_dir = os.path.abspath(os.path.join('.', os.pardir)) + os.path.sep    # pipeline/ (project folder)
    if debug:
        print('[DEBUG] parent_dir: ' + parent_dir)
    config_dir = parent_dir + 'config' + os.path.sep    # pipeline/config/
    data_dir = parent_dir + 'data' + os.path.sep        # pipeline/data/

    # validate config file exists
    campaign_config = config_dir + args.config
    if not os.path.exists(campaign_config):
        print("The File does not exist: " + campaign_config)
        sys.exit('invalid config file')

    # timestamp format
    timestamp_format = '%Y-%m-%d-%H-%M-%S'
    hr_grain_format = '%Y-%m-%d-%H'
    min_grain_format = '%Y-%m-%d-%H-%M'

    # data structure
    field_length = 42
    expected_col_count = 10

    # read in configuration file
    # initialize lists
    init_lists(config_read(campaign_config))

    # globals
    row_num = 0
    test_num = 0
    test_row = 0
    ua_suffix = 0
    batch_count = 1
    expected_row_count = 0
    failed_trans_rows = ''
    app_version = "unknown"
    app_deploytime = "unknown"
    app_deployuser = "unknown"

    # get config data from marathon
    get_pipeline_version()
    get_pipeline_deploytime()
    get_pipeline_deployuser()
    print('app_version:' + '\t' + app_version)
    print('app_deploytime:' + '\t' + app_deploytime)
    print('app_deployuser:' + '\t' + app_deployuser)

    # more setup
    state_get()
    test_time_stamp = datetime.datetime.now()
    run_inc()

    cookielog_time = test_time_stamp.strftime(min_grain_format)

    # write test info to terminal
    print('--------------------------------------------------------------------------------')
    print(Fore.WHITE + 'Test:       ' + args.test)
    print(Fore.WHITE + 'Config:     ' + args.config)
    print(Fore.WHITE + 'State:      [bid=' + bid_get() + ', ip=' + ip_get() + ']')
    print(Fore.WHITE + 'Timestamp:  ' + test_time_stamp.strftime(timestamp_format) + ' (UTC)')
    print(Fore.WHITE + 'Log Time:   ' + cookielog_time)
    print(Fore.WHITE + 'Run Number: ' + str(run_num))
    print(Fore.WHITE + 'Pipeline v: ' + str(app_version) + Style.RESET_ALL)

# -----------------------------------------------------------
def process_row(row):
    global trans
    global row_num
    global test_num
    global test_row
    global ua_suffix
    global expected_row_count

    row_num += 1
    # check for 'comment' row (starts with '#')
    if row[0][:1] == "#":
        # check for 'test' row
        if row[0][:5].lower() == "#test" or row[0][:6].lower() == "# test":
            test_num += 1
            test_row = 0
            return
        else:  # just a comment
            return
    else:  # we have a data row
        test_row += 1
        if len(row) < expected_col_count:
            sys.exit("Invalid data row, column count is less than " + str(expected_col_count))

    # process each column in the data row
    if debug:
        print('[DEBUG] ------------------------------------------------------------------------')
        print("[DEBUG] test_num:" + str(test_num) + ", test_row:" + str(test_row) + ", row_num:" + str(row_num))

    trans_type = row[0].lower()
    bid_in = row[1].lower()
    ip_in = row[2].lower()
    ua_in = row[3].lower()
    d_ago = row[4]
    h_ago = row[5]
    m_ago = row[6]
    s_ago = row[7]
    bot_expected = row[8]
    batch = row[9]

    if debug:
        print('[DEBUG] --------------------------------')
        print("[DEBUG] trans_type: " + trans_type)
        print("[DEBUG] bid_in: " + bid_in)
        print("[DEBUG] ip_in: " + ip_in)
        print("[DEBUG] ua_in: " + ua_in)
        print("[DEBUG] d_ago: " + d_ago)
        print("[DEBUG] h_ago: " + h_ago)
        print("[DEBUG] m_ago: " + m_ago)
        print("[DEBUG] s_ago: " + s_ago)
        print("[DEBUG] bot_expected: " + bot_expected)
        print("[DEBUG] batch: " + batch)
        print('[DEBUG] --------------------------------')

    # --------------------------------------------------
    # Create a list based on the tran_type (I,C,P)
    # --------------------------------------------------
    if trans_type == "c":
        trans = list(c_list)
    elif trans_type == "i":
        trans = list(i_list)
    elif trans_type == "p":
        trans = list(p_list)
    else:
        sys.exit('Invalid trans_type: ' + trans_type)

    # set f1, f2 (primary_cookie, cookie_status)
    if bid_in == 'n' or bid_in == 'new':
        bid_inc()
        trans[1] = trans[22] + '-' + str(bid_get())  # f1
        trans[2] = 'N'                               # f2
    elif bid_in == 'o' or bid_in == 'old':
        trans[1] = trans[22] + '-' + str(bid_get())  # f1
        trans[2] = 'O'                               # f2
    else:
        trans[1] = bid_in                            # f1
        trans[2] = 'O'                               # f2

    # set f4 (remote_ip)
    if ip_in == 'n' or ip_in == 'new':
        ip_inc()
        trans[4] = ip_get()  # f4
    elif ip_in == 'o' or ip_in == 'old':
        trans[4] = ip_get()  # f4
    else:
        trans[4] = ip_in     # f4

    # set f9 (trans_time)
    trans[9] = trans_time_get(test_time_stamp, int(d_ago), int(h_ago), int(m_ago), int(s_ago))  # f9

    # set f13 (user_agent)
    if ua_in == 'n' or ua_in == 'new':
        ua_suffix += 1
        trans[13] = str(ua_list[0]) + str(ua_suffix)
    elif ua_in == 'o' or ua_in == 'old':
        trans[13] = str(ua_list[0]) + str(ua_suffix)
    elif ua_in == 'iab':
        ua_suffix = 0
        trans[13] = str(ua_list[1]) + str(ua_suffix)
    elif ua_in == 'iabn':
        ua_suffix += 1
        trans[13] = str(ua_list[1]) + str(ua_suffix)
    else:
        ua_suffix = int(ua_in)
        trans[13] = str(ua_list[0]) + str(ua_suffix)

    # set f37 (tnx_id)
    trans[37] = uuid_get()

    # set f16
    trans[16] = trans[16].replace('<automatedTime>', test_time_stamp.strftime(timestamp_format))
    trans[16] = trans[16].replace('<bid>', trans[1])
    trans[16] = trans[16].replace('<tnx_id>', trans[37])
    trans[16] = trans[16].replace('<automatedBot>', bot_expected)
    trans[16] = trans[16].replace('<automatedTestCase>', str(test_num))
    trans[16] = trans[16].replace('<automatedRow>', str(row_num))

    # set f21
    trans[21] = trans[21].replace('<automatedTime>', test_time_stamp.strftime(timestamp_format))
    trans[21] = trans[21].replace('<bid>', trans[1])
    trans[21] = trans[21].replace('<tnx_id>', trans[37])
    trans[21] = trans[21].replace('<automatedBot>', bot_expected)
    trans[21] = trans[21].replace('<automatedTestCase>', str(test_num))
    trans[21] = trans[21].replace('<automatedRow>', str(row_num))

    # write cookie log row
    expected_row_count += 1
    if do_s3_format:
        write_s3_row(trans, batch)         # generates .gz by hour_id
    else:
        write_cookie_log_row(trans, batch)  # generates cookie log files

# -----------------------------------------------------------
def config_read(cfg_file):
    config_out = configparser.ConfigParser(interpolation=ExtendedInterpolation())
    config_out.read(cfg_file)
    return config_out

# -----------------------------------------------------------
def init_lists(config_in):
    # impressions
    global i_list
    i_list = [None] * (field_length + 1)
    i_list[0] = field_length
    # clicks
    global c_list
    c_list = [None] * (field_length + 1)
    c_list[0] = field_length
    # pings
    global p_list
    p_list = [None] * (field_length + 1)
    p_list[0] = field_length
    # user agents
    global ua_list
    ua_list = [None] * 2
    # environment
    global env_list
    env_list = [None] * 5

    global s3_upload_dir
    global hive_server

    config_section_get_f_values("impression", config_in, i_list)
    config_section_get_f_values("click", config_in, c_list)
    config_section_get_f_values("ping", config_in, p_list)
    my_ua = config_in['ua']
    ua_list[0] = my_ua['firefox']
    ua_list[1] = my_ua['googlebot']
    my_env = config_in['environment']
    env_list[0] = my_env['env']
    env_list[1] = my_env['server_id']
    env_list[2] = my_env['domain']
    env_list[3] = my_env['ad_server_int']
    env_list[4] = my_env['ad_server_ext']
    s3_upload_dir = my_env['s3_te_filtered_cookie_log_bucket']
    hive_server = my_env['hive_server']

# -----------------------------------------------------------
def config_section_get_f_values(section, config_in, list_in):
    for index in range(1, field_length + 1):
        list_in[index] = config_section_map(section, config_in)["f" + str(index)]

# -----------------------------------------------------------
def config_section_map(section, config_in):
    dict1 = {}
    options = config_in.options(section)
    for option in options:
        try:
            dict1[option] = config_in.get(section, option)
            if dict1[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

# -----------------------------------------------------------
def config_print_tsv(list_in):
    print(*list_in[1:], sep='\t')

# -----------------------------------------------------------
def run_inc():
    global run_num
    run_num += 1

# -----------------------------------------------------------
def state_get():
    global bid
    global ip1
    global ip2
    global ip3
    global ip4
    global run_num

    # read state from google drive cloud
    state = google_drive_api.read_file_as_list(gd_state_file_id)
    bid = int(state[0])
    ip1 = int(state[1])
    ip2 = int(state[2])
    ip3 = int(state[3])
    ip4 = int(state[4])
    run_num = int(state[5])

# -----------------------------------------------------------
def state_set():
    global data_dir

    print('--------------------------------------------------------------------------------')
    print('Save State: [bid=' + bid_get() + ', ip=' + ip_get() + ']')
    if do_save_state:
        state = open(data_dir + 'state.txt', 'w')
        state.write(str(bid) + os.linesep)
        state.write(str(ip1) + os.linesep)
        state.write(str(ip2) + os.linesep)
        state.write(str(ip3) + os.linesep)
        state.write(str(ip4) + os.linesep)
        state.write(str(run_num) + os.linesep)
        state.close()

        # update google drive file in the cloud
        google_drive_api.update_file_from_file(data_dir + 'state.txt', gd_state_file_id)
    else:
        print(Back.RED + 'SAVE STATE IS TURNED OFF!!!')
    # print('----------------------------------------')

# -----------------------------------------------------------
def bid_get():
    return str(bid)

# -----------------------------------------------------------
def bid_inc():
    global bid
    bid += 1
    return bid_get()

# -----------------------------------------------------------
def ip_get():
    return str(ip1) + '.' + str(ip2) + '.' + str(ip3) + '.' + str(ip4)

# -----------------------------------------------------------
def ip_inc():
    # global ip1
    global ip2
    global ip3
    global ip4
    ip4 += 1
    if ip4 > 255:
        ip4 = 0
        ip3 += 1
        if ip3 > 255:
            ip3 = 0
            ip2 += 1
            if ip2 > 255:
                ip2 = 0
                # ip1 = 0  # we always want ip to start with 0.
    return ip_get()

# -----------------------------------------------------------
def uuid_get():
    return str(uuid.uuid1())

# -----------------------------------------------------------
def trans_time_get(reference_time, days_ago, hours_ago, minutes_ago, seconds_ago):
    time = reference_time - datetime.timedelta(days=days_ago, hours=hours_ago, minutes=minutes_ago, seconds=seconds_ago)
    time_formatted = time.strftime(timestamp_format)
    return time_formatted

# -----------------------------------------------------------
def write_cookie_log_row(list_in, batch_in):
    global file_stamp
    global do_batches
    global cookie_log_batch
    global batch_count
    if not do_batches:
        batch_in = 1
        batch_count = 1
    else:
        if int(batch_in) > int(batch_count):
            batch_count = batch_in
    # determine cookie log file name
    tran_ts = list_in[9]    # get the transaction timestamp
    hr_id = tran_ts[:13]    # get the left 13 chars (YYYY-MM-DD-HH)
    cookie_log_batch = test_time_stamp.strftime(timestamp_format)    # timestamp of test run
    cookie_log_batch = cookie_log_batch[:16]    # YYYY-MM-DD-HH-MM
    file_stamp = cookie_log_batch + '_server9' + str(batch_in)
    #file_out_name = hr_id + '_cookie_log_' + file_stamp
    file_out_name = 'cookie_log.' + cookie_log_batch + '_server9' + str(batch_in)

    #file_out = open(data_dir + 'run-' + str(run_num) + '.' + args.test + '.' + env_list[0] + '.' + str(batch_in) + '.' + test_time_stamp.strftime(timestamp_format) + '.out', 'a')
    file_out = open(data_dir + file_out_name, 'a')
    line_out = '\t'.join(list_in[1:])
    file_out.write(line_out + os.linesep)
    file_out.close()
    if debug:
        print(*list_in[1:], sep='\t')

# -----------------------------------------------------------
def write_s3_row(list_in, batch_in):
    global file_stamp
    global do_batches
    global cookie_log_batch
    global batch_count
    if not do_batches:
        batch_in = 1
        batch_count = 1
    else:
        if int(batch_in) > int(batch_count):
            batch_count = batch_in
    # determine s3 filename
    tran_ts = list_in[9]    # get the transaction timestamp
    hr_id = tran_ts[:13]    # get the left 13 chars (YYYY-MM-DD-HH)
    cookie_log_batch = test_time_stamp.strftime(timestamp_format)    # timestamp of test run
    cookie_log_batch = cookie_log_batch[:16]    # YYYY-MM-DD-HH-MM
    file_stamp = cookie_log_batch + '_server9' + str(batch_in)
    file_out_name = hr_id + '_cookie_log_' + file_stamp

    file_out = open(data_dir + file_out_name, 'a')
    line_out = '\t'.join(list_in[1:])
    file_out.write(line_out + os.linesep)
    file_out.close()
    if debug:
        print(*list_in[1:], sep='\t')

# -----------------------------------------------------------
def get_files_regex():
    global re_files
    global cookie_log_batch

    if do_s3_format:
        re_files = data_dir + 'archive/*' + cookie_log_batch + '_server9?'
    else:
        #re_files = data_dir + 'run-' + str(run_num) + '.' + args.test + '.' + env_list[0] + '.*'
        #re_files = data_dir + 'cookie_log.' + cookie_log_batch + '_server9?.gz'
        re_files = data_dir + '*' + cookie_log_batch + '*'
    # print('re_files: ' + re_files)

# -----------------------------------------------------------
def sort_files():
    global data_dir
    match_str = ''

    if do_s3_format:
        match_str = '*server9?'
    else:
        match_str = 'run-' + str(run_num) + '*'

    for file in os.listdir(data_dir):
        if fnmatch.fnmatch(file, match_str):
            # print(file)
            with open(data_dir + file) as f_in:
                lines = [line.split('\t') for line in f_in]
                lines.sort(key=itemgetter(8))
                f_in.close()
            # with open(data_dir + file + '.sorted', 'w') as f_out:
            with open(data_dir + file, 'w') as f_out:
                for el in lines:
                    f_out.write('{0}'.format('\t'.join(el)))
                f_out.close()

# -----------------------------------------------------------
# TODO: add record to dynamodb
# TODO: put record on kinesis queue
# TODO: This could be more efficient.  It iterates each file for each batch.
def upload_files_s3():
    # upload files to s3 bucket
    if do_upload:
        global data_dir
        global s3_upload_dir
        global batch_count
        global cookielog_time

        sleep_between_uploads = 1
        sleep_between_batches = 10

        print('==================== Uploading Batches ====================')

        # iterate batches
        for batch_num in range(1, int(batch_count)+1):
            print(' > uploading batch ' + str(batch_num) + ' of ' + str(batch_count) + '...')

            for file in os.listdir(data_dir):
                file_match = '*' + cookielog_time + '_server9' + str(batch_num) + '.gz'
                # print('looking for: ' + file_match)

                # iterate each file..
                if fnmatch.fnmatch(file, file_match):
                    # TODO: upload to /logs/cookie-log/filtered (uncompressed) INSTEAD of uploading to s3, since the lambda is no longer used.

                    # prepare aws command
                    year = file[:4]
                    month = file[5:7]
                    day = file[8:10]
                    hour = file[11:13]
                    server = file[42:50]
                    s3_file_path = 's3://' + s3_upload_dir + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + server + '/' + file

                    # upload file to s3
                    aws_cmd = 'aws s3 cp ../data/' + file + ' ' + s3_file_path
                    if do_print_aws: print(aws_cmd)
                    process = subprocess.Popen(aws_cmd.split(), stdout=subprocess.PIPE)
                    output = process.communicate()[0]
                    if do_print_aws: print(output)

                    # delete data
                    os.remove(data_dir + file)
                    shutil.move(data_dir + file[:-3], data_dir + 'archive/')

                    # sleep between uploads
                    time.sleep(sleep_between_uploads)

            # sleep between batches
            if int(batch_num) < int(batch_count):
                print(' > sleeping ' + str(sleep_between_batches) + ' seconds...')
                time.sleep(sleep_between_batches)
    else:
        print(Back.RED + 'UPLOAD IS TURNED OFF!!!')

# -----------------------------------------------------------
def gzip_files():
    global data_dir
    global cookie_log_batch
    for file in os.listdir(data_dir):
        if fnmatch.fnmatch(file, '*' + cookie_log_batch + '*'):
            with open(data_dir + file, 'rb') as f_in:
                with gzip.open(f_in.name + '.gz', 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
                f_out.close
            f_in.close()

# -----------------------------------------------------------
def upload_files():
    # upload files to ad servers
    global re_files

    print('==================== Uploading Batches to AdServer ====================')

    # # get the current cookie log from the adserver
    # print('----------------------------------------')
    # print('get the current cookie log file..')
    # cmd = 'ssh ' + hive_user + '@' + env_list[4] + ' ls -tr /logs/cookie_log/ | grep cookie_log | head -n1'
    # print(cmd)
    # cookie_log_file = subprocess.check_output(cmd, shell=True).decode().strip()
    # cookie_log_file = '/logs/cookie_log/' + cookie_log_file
    # print('current cookie_log_file: "' + cookie_log_file + '"')

    # upload all the batches to the adserver..
    #cmd = 'scp ' + re_files + ' ' + getpass.getuser() + '@' + env_list[4] + ':/tmp'
    cmd = 'scp ' + re_files + ' prodman@' + env_list[4] + ':/logs/cookie_log'
    print(cmd)
    if do_upload:
        os.system(cmd)
        # # delete data
        # os.remove(data_dir + 'cookie_log.*')
    else:
        print(Back.RED + 'UPLOAD IS TURNED OFF!!!')

# -----------------------------------------------------------
def gen_hive():
    global expected_row_count
    global run_num
    global env_list
    global data_dir
    global app_version
    global data_dir
    global gd_hive_folder_id

    # determine hadoop table to check
    if env_list[0] == 'DEV':
        hadoop_table = 'ods_stage'
    else:
        hadoop_table = 'ods_s3_stage'

    # generate hive queries
    bash_cmd = "HOUR_IDS=$(cat " + re_files + " | cut -f9 | awk '{print substr($0,0,13)}' | tr -d '-' | sort | uniq); " \
               "HOUR_IDS=`echo $HOUR_IDS | tr ' ' ','`; echo $HOUR_IDS"
    bash_cmd = "HOUR_IDS=$(cat " + re_files + " | cut -f9 | awk '{print substr($0,0,14)}' | tr -d '-' | sort | uniq); " \
                                              "HOUR_IDS=`echo $HOUR_IDS | tr ' ' ','`; echo $HOUR_IDS"
    hour_ids = subprocess.check_output(bash_cmd, shell=True).decode('ascii').strip()
    print('bash_cmd: "' + bash_cmd + '"')
    print('hour_ids: "' + hour_ids + '"')

    # ----------------------------------
    # output hive queries to terminal
    # ----------------------------------
    print('----------------------------------------')
    print('= Generating Hive Queries =')

    hive_count = 'hive -S -e "select count(*) from ' + hadoop_table + ' where hour_id in (' + hour_ids + ') and ' \
                 'f16[\'automatedTime\'] = \'' + test_time_stamp.strftime(timestamp_format) + '\'"'
    print('All Transactions Count: ' + str(expected_row_count))
    print(Style.DIM + Fore.YELLOW + hive_count + Style.RESET_ALL)

    hive_failed = 'hive -S -e "select trans_type, spider_bot, f16[\'automatedBot\'], f16[\'automatedTestCase\'], ' \
                  'f16[\'automatedRow\'] from ' + hadoop_table + ' where hour_id in (' + hour_ids + ') and ' \
                  'spider_bot != f16[\'automatedBot\'] and f16[\'automatedTime\'] = \'' + \
                  test_time_stamp.strftime(timestamp_format) + '\'" | sort -k5 -n'

    print('Failed Transactions:')
    print(Fore.YELLOW + hive_failed + Style.RESET_ALL)

    hive_all = 'hive -S -e "select trans_type, spider_bot, f16[\'automatedBot\'], f16[\'automatedTestCase\'], ' \
               'f16[\'automatedRow\'] from ' + hadoop_table + ' where hour_id in (' + hour_ids + ') and ' \
               'f16[\'automatedTime\'] = \'' + test_time_stamp.strftime(timestamp_format) + '\'" | sort -k5 -n'

    print('Passed and Failed (all) Transactions:')
    print(Style.DIM + Fore.YELLOW + hive_all + Style.RESET_ALL)

    # ----------------------------------
    # generate bash scripts
    # ----------------------------------

    info = 'Run: ' + str(run_num) + ', Env: ' + env_list[0] + ', Test: ' + test_time_stamp.strftime(timestamp_format)

    # transaction counts
    file_out = open('./util/hive_count.sh', 'w')
    file_out.write('#!/usr/bin/env bash' + os.linesep)
    file_out.write('#echo "' + info + '"' + os.linesep)
    file_out.write('#echo "Expected transaction count: ' + str(expected_row_count) + '"' + os.linesep)
    file_out.write('#echo "Getting actual transaction count..."' + os.linesep)
    file_out.write(hive_count + os.linesep)
    file_out.close()

    # failed transactions
    file_out = open('./util/hive_get_failed_trans.sh', 'w')
    file_out.write('#!/usr/bin/env bash' + os.linesep)
    file_out.write('#echo "' + info + '"' + os.linesep)
    file_out.write('#echo "Running hive to get failed transactions from ods_stage..."' + os.linesep)
    file_out.write(hive_failed + os.linesep)
    file_out.close()

    # ----------------------------------
    # write hive queries to file
    # ----------------------------------
    # write failed result file
    res_file = args.test                                # to generate .tsv file
    res_file = 'run' + str(run_num) + '_' + env_list[0] + '_' + app_version + '_' + test_time_stamp.strftime(timestamp_format) + '_' + res_file + '.txt'
    hive_file = data_dir + res_file
    with open(hive_file, 'a') as hive_file_out:
        hive_file_out.write('--------------------------------------------------------------------------------' + os.linesep)
        hive_file_out.write(info + os.linesep)
        hive_file_out.write('----------------------------------------' + os.linesep)
        hive_file_out.write('   All Transactions Count: ' + str(expected_row_count) + os.linesep)
        hive_file_out.write(hive_count + os.linesep)
        hive_file_out.write('----------------------------------------' + os.linesep)
        hive_file_out.write('   Failed Transactions:' + os.linesep)
        hive_file_out.write(hive_failed + os.linesep)
        hive_file_out.write('----------------------------------------' + os.linesep)
        hive_file_out.write('   Passed & Failed Trans (all):' + os.linesep)
        hive_file_out.write(hive_all + os.linesep)
        hive_file_out.write('' + os.linesep)
        hive_file_out.close()

    # upload to google drive in the cloud and clean up local file
    google_drive_api.upload_file(data_dir, res_file, gd_hive_folder_id)
    os.remove(hive_file)

# -----------------------------------------------------------
# process each 'data driven' row in the test file
def run_test():
    # read lines from test file
    try:
        lines = google_sheets_api.read_worksheet_range(args.testFileId, args.test, 1, 1, 0, 10)
    except:  # catch *all* exceptions
        print("Exception reading data: " + sys.exc_info()[0])
        exit(False)
    # process each line in test file
    for line in lines:
        if debug: print("[DEBUG] " + str(line))
        # call method to process row
        process_row(line)

# -----------------------------------------------------------
def wait_for_ods_stage():
    # wait for all transactions to make it to ods_stage
    global expected_row_count
    global run_num
    global env_list
    global hive_server
    global app_version
    global data_dir
    global gd_results_folder_id

    # local vars
    attempts_max = 15       # number of times to try before giving up
    attempts = 0            # number times tried
    actual_row_count = -1   # number of transactions returned

    print('----------------------------------------')
    print('Waiting for transactions to arrive in ods_stage on ' + hive_server + '...')
    while actual_row_count != expected_row_count and attempts < attempts_max:
        attempts += 1
        print(' > attempt ' + str(attempts) + ' of ' + str(attempts_max) + '...')
        actual_row_count = get_trans_count_ods_stage()
        print('   - actual: ' + str(actual_row_count) + ', expected: ' + str(expected_row_count))
        if int(actual_row_count) == int(expected_row_count):
            print(Fore.GREEN + 'SUCCESS: All transactions have arrived in ods_stage!' + Style.RESET_ALL)
            return True
        else:
            print('   - sleeping for 60 seconds...')
            time.sleep(60)

    # max attempts exceeded, bail out
    print(Back.RED + Fore.WHITE + 'ERROR: Max attempts exceeded!' + Style.RESET_ALL)

    # write failed result file
    res_file = args.test                                # to generate .tsv file
    res_file = 'run' + str(run_num) + '_' + env_list[0] + '_' + app_version + '_' + test_time_stamp.strftime(timestamp_format) + '_' + res_file + '.tsv'
    f_results = data_dir + res_file
    with open(f_results, "a") as fout:
        fout.write('fail' + '\t' + 'timeout' + '\t' + str(actual_row_count) + '\t' + str(expected_row_count))

    # upload to google drive in the cloud and clean up local file
    google_drive_api.upload_file(data_dir, res_file, gd_results_folder_id)
    os.remove(f_results)

    return False

# -----------------------------------------------------------
def get_trans_count_ods_stage():
    global hive_server

    # current user
    user = getpass.getuser()

    # ssh <user>@hive_server 'bash -s' < hive_count.sh
    #bash_cmd = "ssh " + user + "@" + hive_server + " 'bash -s' < ./util/hive_count.sh"
    # with open('./util/hive_count.sh', 'r') as fin:
    #     print(fin.read())
    bash_cmd = "ssh " + hive_user + "@" + hive_server + " 'bash -s' < ./util/hive_count.sh"
    cmd_result = subprocess.check_output(bash_cmd, shell=True).decode('ascii').strip()
    # print('Actual transaction count: ' + cmd_result)
    return cmd_result

# -----------------------------------------------------------
def get_failed_trans_ods_stage():
    global failed_trans_rows
    global hive_server
    global run_num
    global env_list
    global app_version
    global app_deploytime       # 'deploy_time' of the pipeline app
    global app_deployuser       # 'deploy_user' of the pipeline app
    global data_dir
    global gd_results_folder_id

    # current user
    user = getpass.getuser()

    # results file
    res_file = args.test                                # to generate .tsv file
    res_file = 'run' + str(run_num) + '_' + env_list[0] + '_' + app_version + '_' + test_time_stamp.strftime(timestamp_format) + '_' + res_file + '_' + app_deploytime + '_' + app_deployuser + '.tsv'
    f_results = data_dir + res_file

    print('----------------------------------------')
    print('Getting test results...')
    #bash_cmd = "ssh " + user + "@" + hive_server + " 'bash -s' < ./util/hive_get_failed_trans.sh > " + '"' + f_results + '"'
    bash_cmd = "ssh " + hive_user + "@" + hive_server + " 'bash -s' < ./util/hive_get_failed_trans.sh > " + '"' + f_results + '"'
    failed_trans_rows = subprocess.check_output(bash_cmd, shell=True).decode('ascii').strip()

    # get results file size (0=pass, else fail)
    fsize = os.path.getsize(f_results)

    # pass
    if fsize == 0:
        print(Back.GREEN + Fore.WHITE + 'Test PASSED!' + Style.RESET_ALL)
    # fail
    else:
        print(Back.RED + Fore.BLACK + 'Test FAILED!' + Style.RESET_ALL)

        # read results file and write to terminal
        with open(f_results, 'r') as fin:
            failed_trans_rows = fin.read()
        print('Failed transactions: ' + Back.YELLOW + Fore.BLACK + Style.BRIGHT + 'run ' + str(run_num) + Style.RESET_ALL)
        print(failed_trans_rows)

    # copy results to google drive
    print(' > uploading result file to google drive: ' + str(res_file))
    google_drive_api.upload_file(data_dir, res_file, gd_results_folder_id)

    os.remove(f_results)

    # result
    if fsize == 0:
        return True
    else:
        return False

# -----------------------------------------------------------
def get_pipeline_version():
    global env_list
    global app_version

    if env_list[0] == "DEV":
        app_version = subprocess.check_output('./util/get-version-pipeline-DEV.sh', shell=True).decode('ascii').strip()
    elif env_list[0] == "STG":
        app_version = subprocess.check_output('./util/get-version-pipeline-STG.sh', shell=True).decode('ascii').strip()

    # print('app_version: ' + app_version)
    return app_version

# -----------------------------------------------------------
def get_pipeline_deploytime():
    global env_list
    global app_deploytime

    if env_list[0] == "DEV":
        app_deploytime = subprocess.check_output('./util/get-deploytime-pipeline-DEV.sh', shell=True).decode('ascii').strip()
    elif env_list[0] == "STG":
        app_deploytime = subprocess.check_output('./util/get-deploytime-pipeline-STG.sh', shell=True).decode('ascii').strip()

    # print('app_deploytime: ' + app_deploytime)
    return app_deploytime

# -----------------------------------------------------------
def get_pipeline_deployuser():
    global env_list
    global app_deployuser

    if env_list[0] == "DEV":
        app_deployuser = subprocess.check_output('./util/get-deployuser-pipeline-DEV.sh', shell=True).decode('ascii').strip()
    elif env_list[0] == "STG":
        app_deployuser = subprocess.check_output('./util/get-deployuser-pipeline-STG.sh', shell=True).decode('ascii').strip()

    # print('app_deployuser: ' + app_deployuser)
    return app_deployuser


# -----------------------------------------------------------

# --------------------------------------------------------------------------------------------------
