#!/usr/bin/env bash
#
# Script waits for "order_id" transactions to arrive
#   in ods_stage.  Any unexpected data is logged
#   as a failure.
#

# environment
#env="dev"
env="stg"

expected_count=40
max_tries=30

# colors
normal="\e[0m"
bold="\e[1m"
fgPass="\e[97m"
bgPass="\e[42m"
fgFail="\e[97m"
bgFail="\e[101m"

echo "========================================"
echo -e "=  \e[93mVerification for 'order_id' tests${normal}   ="
echo "========================================"
echo "env:         ${env}"


# data files
parentdir=$(dirname `pwd`)
datadir="${parentdir}/data/order_id"

hive_count_file="${datadir}/hive_count-${env}.sh"
hive_results_file="${datadir}/hive_results-${env}.sh"

get_hive_count_file="${datadir}/get-hive_count-${env}.sh"
get_hive_results_file="${datadir}/get-hive_results-${env}.sh"

# environment dependent variables
hive_server=""
if [ "${env}" = "dev" ]; then
    hive_server="havok"
    hive_table="ods_stage"
elif [ "${env}" = "stg" ]; then
    hive_server="runamuck2"
    hive_table="ods_s3_stage"
fi
echo "hive_server: ${hive_server}"
echo "hive_table:  ${hive_table}"
echo "========================================"

# get the transaction count
echo "Waiting for transactions to arrive in ${hive_table} on ${hive_server}..."
loop_count=0
let "loop_count += 1"
shfile="${datadir}/get-hive_count-${env}.sh"
count=`source ${shfile}`
echo " > attempt: ${loop_count} of ${max_tries}, actual count: ${count}, expected count: ${expected_count}"
while [ $count -ne $expected_count ]; do
    if [ $loop_count -eq $max_tries ]; then
        echo -e "${fgFail}${bgFail}${bold}FAIL: Max attempts reached, bailing out!!!${normal}"
        exit 1
    fi
    let "loop_count += 1"
    count=`source ${shfile}`
    echo " > attempt: ${loop_count} of ${max_tries}, actual count: ${count}, expected count: ${expected_count}"
done
echo -e "${fgPass}${bgPass}${bold}OK:${normal} All transactions have arrived!"

# get failed transactions
echo "Getting list of failed transactions..."
shfile="${datadir}/get-hive_results-${env}.sh"
res=`source ${shfile}`
#echo "res: '${res}'"
if [[ ! $res ]]; then
    # Pass
    echo -e "${fgPass}${bgPass}${bold}ALL 'order_id' TESTS PASSED!${normal}"
else
    # Fail
    echo -e "${fgFail}${bgFail}${bold}FAIL: The following tests failed!!!${normal}"
    echo "${res}"
    exit 2
fi

# all done
echo "========================================"
