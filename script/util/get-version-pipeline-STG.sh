#!/usr/bin/env bash
V=$( curl -s http://platform-stg.trueffect.net:8080/v2/apps//platform-pipeline-stg?embed=app.taskStats | jq -r ".app.container.docker.image" )
VERSION="${V##*:}"
REPO="${V%%:*}"
#echo $VERSION $REPO
echo $VERSION