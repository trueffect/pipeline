#!/usr/bin/env bash


# note: path is relative to calling script, not this script
echo Deleting ../data/archive/ files
rm -rf ../data/archive/*

echo Deleting ../data/ files
rm -rf ../data/cookie_log.*
