#!/usr/bin/env bash
V=$( curl -s http://platform-dev.trueffect.net:8080/v2/apps//platform-pipeline-dev?embed=app.taskStats | jq -r ".app.container.docker.image" )
VERSION="${V##*:}"
REPO="${V%%:*}"
#echo $VERSION $REPO
echo $VERSION
