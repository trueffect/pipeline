#!/usr/local/bin/python3
#
# --------------------------------------------------------------------------------------------------
#
#   Purpose:
#       This script generates cookie log transactions.  The generated files are used to test the
#       data processing pipeline.  The files are to be appended to the ad-server's cookie log.
#
#   See 'README.md' documentation for more info.
#
# --------------------------------------------------------------------------------------------------
#

import lib
import time


# parse script arguments
lib.parse_args()

# declare and initialize globals
lib.init_globals()

# run test
lib.run_test()

# save state
lib.state_set()

# get the regex for the files
lib.get_files_regex()

# sort and upload files
lib.sort_files()
if lib.do_s3_format:
    lib.gzip_files()
    lib.upload_files_s3()
else:
    #lib.gzip_files()
    lib.upload_files()

# give data a chance to download to ods_s3_stage
if lib.do_upload:
    sleep_secs = 120
    print(' > sleeping ' + str(sleep_secs) + ' seconds to give data a chance to download to ods_stage...')
    time.sleep(sleep_secs)

# generate hive queries
lib.gen_hive()

# wait for all transactions to exist in ods_stage
if lib.do_upload:
    if lib.wait_for_ods_stage():
        # get failed transactions
        if lib.get_failed_trans_ods_stage():
            print('Test Passed')
            exit(0)
        else:
            print('Test Failed')
            exit(1)
    # error, we timed out
    else:
        print('Timed out waiting for transactions to arrive in ods_stage!')
        print('Test Failed')
        exit(2)

print('----------------------------------------')

# --------------------------------------------------------------------------------------------------
