#!/usr/bin/env bash
#
# This script removes items from the signature store
#

IPADDRS=$(cat ip_cleanup.txt)
#echo $IPADDRS

# create an empty file
> ip_cleanup_data.txt

# write bid/tnx data to file
for IP in $IPADDRS; do
    #cat ../data/archive/*cidr_black-white.STG* ../data/archive/*server9? | grep $IP | cut -f1,37 | sort >> ip_cleanup_data.txt
    cat ../data/archive/*server9? | grep $IP | cut -f1,37 | sort >> ip_cleanup_data.txt
done

# for each bid/tnx, delete the item from dynamodb signature store
while read LINE; do
    BID=$(echo $LINE | cut -d ' ' -f1)
    TXN=$(echo $LINE | cut -d ' ' -f2)

    KEY="{\"browser_id\":{\"S\":\"$BID\"},\"txn_id\":{\"S\":\"$TXN\"}}"
    AWS_CMD="aws dynamodb delete-item --table-name STG_SIGNATURE_STORE --key $KEY"
    echo $AWS_CMD
    $AWS_CMD
done <ip_cleanup_data.txt
