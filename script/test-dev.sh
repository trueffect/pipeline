#!/usr/bin/env bash
python3 driver.py --test test --testFileId 1NPOyxoWqFmhZ1c-miWlp-2cnQI_T3X9hZP34qnMnai4 --config dev-ext_campaign_9173933.cfg
res=$?

echo "****************************************"
echo "Test Result: $res (0=Pass, !0=Fail)"
echo "****************************************"

# clean up data
./util/delete-data-archive.sh

exit $res
