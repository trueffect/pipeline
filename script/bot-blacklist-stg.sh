#!/usr/bin/env bash
python3 driver.py --test bot-blacklist --testFileId 1NPOyxoWqFmhZ1c-miWlp-2cnQI_T3X9hZP34qnMnai4 --config stg-ext_campaign_5993656.cfg
res=$?

echo "****************************************"
echo "Test Result: $res (0=Pass, !0=Fail)"
echo "****************************************"

# clean up transaction store (only needs to be done for black/white list IP testing)
./ip_cleanup-stg.sh

# clean up data
./util/delete-data-archive.sh

exit $res
