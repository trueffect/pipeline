#!/usr/bin/env bash

# submit 'order_id' transactions
#./order_id-dev-1.sh
#./order_id-stg-1.sh

# submit 'bot' transactions
./bot-default-settings-dev.sh
./bot-override-settings-dev.sh
./bot-blacklist-dev.sh
./bot-whitelist-dev.sh
./bot-default-settings-stg.sh
./bot-override-settings-stg.sh
./bot-blacklist-stg.sh
./bot-whitelist-stg.sh

# verify 'order_id' transactions
#./order_id-dev-2.sh
#./order_id-stg-2.sh
