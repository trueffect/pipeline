#!/usr/bin/env bash
#
# Script uses cURL to generate 'P' records against the adserver.
# Each record tests various transformations for order_id.
#
# Script generates hive queries to verify data in ods_stage.
#

# environment
env="dev"
#env="stg"
echo "========================================"
echo "env:         ${env}"
expected_count="40"

# sec_id is the unique second grain timestamp for the run (to find in hive tables)
sec_id=$(date +"%Y%m%d%H%M%S")
#echo "sec_id: ${sec_id}"

# data files
parentdir=$(dirname `pwd`)
datadir="${parentdir}/data/order_id"
hive_count_file="${datadir}/hive_count-${env}-${sec_id}.sh"
hive_results_file="${datadir}/hive_results-${env}-${sec_id}.sh"

hive_count_file="${datadir}/hive_count-${env}.sh"
hive_results_file="${datadir}/hive_results-${env}.sh"

get_hive_count_file="${datadir}/get-hive_count-${env}.sh"
get_hive_results_file="${datadir}/get-hive_results-${env}.sh"

# environment dependent variables
hive_server=""
domain=""
spacedesc=""
group=""
event=""
if [ "${env}" = "dev" ]; then
    hive_server="havok"
    hive_table="ods_stage"
    domain="http://extdev.adlegend.net"
    spacedesc="9241387_1061349_1x1_1061349_1061349"
    group="gpDE1031"
    event="eventConvWRev"
elif [ "${env}" = "stg" ]; then
    hive_server="runamuck2"
    hive_table="ods_s3_stage"
    domain="http://qa.adlegend.net"
    spacedesc="6069556_1061349_1x1_1061349_1061349"
    group="BeetleGroup"
    event="BeetleEvent"
fi
echo "hive_server: ${hive_server}"
echo "hive_table:  ${hive_table}"
echo "domain:      ${domain}"
echo "spacedesc:   ${spacedesc}"
echo "group:       ${group}"
echo "event:       ${event}"
echo "========================================"
echo "Generating data for order_id fields:"

# hour_id we started with (in case it changes during the run)
hour_id_start=$(date +"%Y%m%d%H")
#echo "hour_id_start: ${hour_id_start}"

# order_id
echo "order_id"
curl -s --cookie "PrefID=789-1001" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=01&expected=-&sec_id=${sec_id}&order_id=" > /dev/null
curl -s --cookie "PrefID=789-1001" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=02&expected=-&order_id=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1001" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=03&expected=1001&sec_id=${sec_id}&order_id=1001" > /dev/null
curl -s --cookie "PrefID=789-1001" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=04&expected=1001&order_id=1001&sec_id=${sec_id}" > /dev/null

# orderid
echo "orderid"
curl -s --cookie "PrefID=789-1002" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=05&expected=-&sec_id=${sec_id}&orderid=" > /dev/null
curl -s --cookie "PrefID=789-1002" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=06&expected=-&orderid=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1002" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=07&expected=1002&sec_id=${sec_id}&orderid=1002" > /dev/null
curl -s --cookie "PrefID=789-1002" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=08&expected=1002&orderid=1002&sec_id=${sec_id}" > /dev/null

# x_orderid
echo "x_orderid"
curl -s --cookie "PrefID=789-1003" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=09&expected=-&sec_id=${sec_id}&x_orderid=" > /dev/null
curl -s --cookie "PrefID=789-1003" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=10&expected=-&x_orderid=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1003" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=11&expected=1003&sec_id=${sec_id}&x_orderid=1003" > /dev/null
curl -s --cookie "PrefID=789-1003" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=12&expected=1003&x_orderid=1003&sec_id=${sec_id}" > /dev/null

# x_order_id
echo "x_order_id"
curl -s --cookie "PrefID=789-1004" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=13&expected=-&sec_id=${sec_id}&x_order_id=" > /dev/null
curl -s --cookie "PrefID=789-1004" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=14&expected=-&x_order_id=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1004" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=15&expected=1004&sec_id=${sec_id}&x_order_id=1004" > /dev/null
curl -s --cookie "PrefID=789-1004" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=16&expected=1004&x_order_id=1004&sec_id=${sec_id}" > /dev/null

# appt_id
echo "appt_id"
curl -s --cookie "PrefID=789-1005" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=17&expected=-&sec_id=${sec_id}&appt_id=" > /dev/null
curl -s --cookie "PrefID=789-1005" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=18&expected=-&appt_id=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1005" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=19&expected=1005&sec_id=${sec_id}&appt_id=1005" > /dev/null
curl -s --cookie "PrefID=789-1005" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=20&expected=1005&appt_id=1005&sec_id=${sec_id}" > /dev/null

# external_id
echo "external_id"
curl -s --cookie "PrefID=789-1006" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=21&expected=-&sec_id=${sec_id}&external_id=" > /dev/null
curl -s --cookie "PrefID=789-1006" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=22&expected=-&external_id=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1006" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=23&expected=1006&sec_id=${sec_id}&external_id=1006" > /dev/null
curl -s --cookie "PrefID=789-1006" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=24&expected=1006&sec_id=${sec_id}&external_id=1006" > /dev/null

# x_appt_id
echo "x_appt_id"
curl -s --cookie "PrefID=789-1007" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=25&expected=-&sec_id=${sec_id}&x_appt_id=" > /dev/null
curl -s --cookie "PrefID=789-1007" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=26&expected=-&x_appt_id=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1007" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=27&expected=1007&sec_id=${sec_id}&x_appt_id=1007" > /dev/null
curl -s --cookie "PrefID=789-1007" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=28&expected=1007&x_appt_id=1007&sec_id=${sec_id}" > /dev/null

# x_external_id
echo "x_external_id"
curl -s --cookie "PrefID=789-1008" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=29&expected=-&sec_id=${sec_id}&x_external_id=" > /dev/null
curl -s --cookie "PrefID=789-1008" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=30&expected=-&x_external_id=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1008" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=31&expected=1008&sec_id=${sec_id}&x_external_id=1008" > /dev/null
curl -s --cookie "PrefID=789-1008" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=32&expected=1008&x_external_id=1008&sec_id=${sec_id}" > /dev/null

# dup
echo "dup"
curl -s --cookie "PrefID=789-1009" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=33&expected=-&sec_id=${sec_id}&order_id=&orderid=" > /dev/null
curl -s --cookie "PrefID=789-1009" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=34&expected=-&order_id=&orderid=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1009" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=35&expected=1009-last&sec_id=${sec_id}&order_id=1009-first&orderid=1009-last" > /dev/null
curl -s --cookie "PrefID=789-1009" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=36&expected=1009-last&order_id=1009-first&orderid=1009-last&sec_id=${sec_id}" > /dev/null

# dup reversed
echo "dup reversed"
curl -s --cookie "PrefID=789-1010" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=37&expected=-&sec_id=${sec_id}&orderid=&order_id=" > /dev/null
curl -s --cookie "PrefID=789-1010" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=38&expected=-&orderid=&order_id=&sec_id=${sec_id}" > /dev/null
curl -s --cookie "PrefID=789-1010" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=39&expected=1010-last&sec_id=${sec_id}&orderid=1010-first&order_id=1010-last" > /dev/null
curl -s --cookie "PrefID=789-1010" "${domain}/ping?spacedesc=${spacedesc}&db_afcr=123&group=${group}&event=${event}&test=40&expected=1010-last&orderid=1010-first&order_id=1010-last&sec_id=${sec_id}" > /dev/null

# hour_id we ended with (in case it changed during the run)
hour_id_end=$(date +"%Y%m%d%H")

# run data
echo "========================================"
echo "sec_id:        ${sec_id}"
echo "hour_id_start: ${hour_id_start}"
echo "hour_id_end:   ${hour_id_end}"
echo ""
echo "hive -S -e \"select count(*) from ${hive_table} where hour_id in ('${hour_id_start}','${hour_id_end}') and f16['sec_id'] = '${sec_id}'\""
echo "hive -S -e \"select f16['test'], order_id, f16['expected'] from ${hive_table} where hour_id in ('${hour_id_start}','${hour_id_end}') and f16['sec_id'] = '${sec_id}' and order_id != f16['expected']\""

# write hive query for transaction count
echo "#!/usr/bin/env bash" > "${hive_count_file}"
echo "#env:           ${env}" >> "${hive_count_file}"
echo "#hive_server:   ${hive_server}" >> "${hive_count_file}"
echo "#hive_table:    ${hive_table}" >> "${hive_results_file}"
echo "#sec_id:        ${sec_id}" >> "${hive_count_file}"
echo "#hour_id_start: ${hour_id_start}" >> "${hive_count_file}"
echo "#hour_id_end:   ${hour_id_end}" >> "${hive_count_file}"
echo "hive -S -e \"select count(*) from ${hive_table} where hour_id in ('${hour_id_start}','${hour_id_end}') and f16['sec_id'] = '${sec_id}'\""  >> "${hive_count_file}"

# write hive query for actual results
echo "#!/usr/bin/env bash" > "${hive_results_file}"
echo "#env:           ${env}" >> "${hive_results_file}"
echo "#hive_server:   ${hive_server}" >> "${hive_results_file}"
echo "#hive_table:    ${hive_table}" >> "${hive_results_file}"
echo "#sec_id:        ${sec_id}" >> "${hive_results_file}"
echo "#hour_id_start: ${hour_id_start}" >> "${hive_results_file}"
echo "#hour_id_end:   ${hour_id_end}" >> "${hive_results_file}"
echo "hive -S -e \"select browser_id, order_id, f16['expected'] from ${hive_table} where hour_id in ('${hour_id_start}','${hour_id_end}') and f16['sec_id'] = '${sec_id}' and order_id != f16['expected']\"" >> "${hive_results_file}"

# write shell commands
echo ""
cmd_hive_count="ssh prodman@${hive_server} 'bash -s' < ${hive_count_file}"
echo "cmd_hive_count: ${cmd_hive_count}"
echo "#!/usr/bin/env bash" > "${get_hive_count_file}"
echo "${cmd_hive_count}" >> "${get_hive_count_file}"

cmd_hive_results="ssh prodman@${hive_server} 'bash -s' < ${hive_results_file}"
echo "cmd_hive_results: ${cmd_hive_results}"
echo "#!/usr/bin/env bash" > "${get_hive_results_file}"
echo "${cmd_hive_results}" >> "${get_hive_results_file}"
