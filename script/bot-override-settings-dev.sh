#!/usr/bin/env bash
python3 driver.py --test bot-override-settings --testFileId 1NPOyxoWqFmhZ1c-miWlp-2cnQI_T3X9hZP34qnMnai4 --config dev-int_campaign_9391166.cfg
res=$?

echo "****************************************"
echo "Test Result: $res (0=Pass, !0=Fail)"
echo "****************************************"

# clean up data
./util/delete-data-archive.sh

exit $res
