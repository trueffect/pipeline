#Pipeline Automation using Docker

The pipeline automation is run using Docker.  This document explains how to setup and run various pipeline tests.

## First Time Setup for Mac OS X

To run on Mac OS X, you need to install docker-machine via the Docker Toolbox.

Do I have docker-machine already installed on my Mac?  Let's take a look and see with the `docker-machine -version` command:

```
$ docker-machine -version

docker-machine version 0.7.0, build a650a40
```

If you get `command not found`, the installation can be found here:
https://www.docker.com/products/docker-toolbox

Be sure to create the default docker-machine container:

`docker-machine create --driver virtualbox default`

## Starting docker-machine and Building the Image

1) `docker-machine start`

2) `docker-machine ssh`

3) cd to the path where the Dockerfile is located, ie. `cd /path-to-projects/pipeline/docker`

4) `docker build -t pipe .` (don't forget the '.')  Note, if you made code changes and the repos are cached, you'll need to try `docker build --no-cache pipe .`

## Running a Test on the Image

You can run a full regression (all individual tests), or you can run any individual test by referencing the following table.

NOTE:  You MUST set your working directory to the project folder (ie. /Users/wbutler/projects/automated-tests).
NOTE:  It is assumed that you started docker-machine and built the image as specified in the previous section above!

| Environment | Test | Docker Command |
|---:|---|---|
| DEV | Full Regression | `docker run pipe /pipeline/script/full_regression-dev.sh` |
| DEV | Bot Filtering - Default Settings | `docker run pipe /pipeline/script/bot-default-settings-dev.sh` |
| DEV | Bot Filtering - Override Settings | `docker run pipe /pipeline/script/bot-override-settings-dev.sh` |
| DEV | Blacklist | `docker run pipe /pipeline/script/bot-blacklist-dev.sh` |
| DEV | Whitelist | `docker run pipe /pipeline/script/bot-whitelist-dev.sh` |
| STG | Full Regression | `docker run pipe /pipeline/script/full_regression-stg.sh` |
| STG | Bot Filtering - Default Settings | `docker run pipe /pipeline/script/bot-default-settings-stg.sh` |
| STG | Bot Filtering - Override Settings | `docker run pipe /pipeline/script/bot-override-settings-stg.sh` |
| STG | Blacklist | `docker run pipe /pipeline/script/bot-blacklist-stg.sh` |
| STG | Whitelist | `docker run pipe /pipeline/script/bot-whitelist-stg.sh` |

## One-time setup of ssh keys on hadoop server

As Prodman, append the following row to the `/home/prodman/.ssh/authorized_keys` file:

`ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCo2WDksbLu4br54+WIm8TM8OctLz8ZRysUjPaos7FGZR6tiDFOUl57H94zpzBGqhnwrp9DIMP00Xwu+q6HECK055VG6KUlwuGWk6fNxmQ5dKJC7ZJp2LSUw//gnwLHMF55my9b7Sq7Sf3XVHofbXk0u5FJcSOG9cy5mefynujKrRMnBIkNJ+KVqtZyskAIKjC/8xcJs368wzNnAAXackLk1igM/LeB9ekgfD6SDWJXm5htf6F6YgngETOtoXNPX/irev33OXrjRzjGsmpxZlTEeqF3Ax8InOTvfVKCLBruXMN8U4Kmdyb3bm4bOhM1qhyeLoeafpb5bPUPqtMh8PLh wbutler@WBUTLER1`
