***

   # AdServer Log Generator Tool

   This tool generates cookie logs, and feeds them into the pipeline.  It verifies the pipeline output by running hive
   queries against ods_stage.

   Developed and tested with MAC OS X.  Windows support is unknown, so you're on your own.

   **Note**: The data and results are stored in Microsoft Excel.  You only need Excel for creating test data and viewing the test results.  This tool does not depend on Excel being installed on the localhost to run the automation.

   ***

   # Table of Contents

   - [Get the Code](#get-the-code)
   - [Install Dependencies](#install-dependencies)
   - [Setup Local Environment](#setup-local-environment)
   - [Running Test Scripts](#running-test-scripts)
   - [How it Works](#how-it-works)
   - [Which Tests do What?  How do I Run a Full Regression?](#which-tests)

   <a name="get-the-code"></a>

   ***

   ## Get the Code

   If you don't have the code, clone it from the git repo (substitute YOUR-ID, ie. wbutler):

   `git clone https://YOUR-ID@stash.trueffect.com:8443/scm/pl/test-automation.git`

   <a name="install-dependencies"></a>

   ***

   ## Install Dependencies

   The following dependencies are required:

     * Python 3.x (created with 3.4.3)
     * AWS CLI
     * Python plugin for IntelliJ
     * colorama
     * pytz

   ### Install python3:
   Install using brew:

   `brew install python3`

   Verify the install by getting the version (yup, that's a capital "V"):

   `python3 -V`

   If you don't get what you expect, you might try opening a new Terminal window.

   ### Install AWS Command Line Interface (CLI)
   Docs are here: [https://aws.amazon.com/cli/]()

   Do I already have it installed?  Run this command and see: `aws --version`

   To install, use pip3:

   `pip3 install awscli`

   To verify install, run command: `aws --version`


   ### Install Python plugin for IntelliJ
   Launch IntelliJ and perform these steps (created using IntelliJ 14):

     1. Select "IntelliJ IDE > Preferences..." from the IntelliJ toolbar
     2. Select "Plugins" from the left navigator
     3. Click "Browse Repositories..." button
     4. Search for "python"
     5. Select 'Python Community Edition'
     6. Click "Install plugin" button
     7. Restart IntelliJ

   ### Import Project with IntelliJ
   Import the project using these steps (created using IntelliJ 14):

     1. Select "File > New > Project from Existing Resources..." from the IntelliJ toolbar
     2. Select path "<your-path-to-project>/platform/test-automation/ad-server-log-generator-tool", click "OK" button
     3. Select "Create project from existing sources", click "Next" button
     4. Enter the 'Project name' (I just used the default), click "Next" button
     5. Click "Next" again (Source files, leave checked)
     6. Click "Finish" (No frameworks detected)
     7. Select "File > Project Structure..." from the IntelliJ toolbar
     8. Select 'Project' under 'Project Settings'.  For the 'Project SDK' choose "Python 3.x.x" in the dropdown, click "OK" button to save change.  If you don't see the Python SDK available to choose, then click "New..." button.  Choose "Python SDK", "Add Local" option.  Choose python3 (run `which python3` command to get the correct path).

   ### Install colorama
   Docs for colorama: [https://pypi.python.org/pypi/colorama]()

   `pip3 install colorama`

   ### Install pytz
   Docs for colorama: [http://pytz.sourceforge.net/]()

   `pip3 install pytz`

   ### Install google-api-python-client

   `pip3 install --upgrade google-api-python-client`

   <a name="setup-local-environment"></a>

   ***

   ### Setup RSA keys
   If you can 'ssh' in to havok AND runamuck2 without using your password, you can skip this section.  When your TE domain password changes, you may need to regenerate the keys with the following steps.

   Setup keys for STG (runamuck2) and DEV (havok).  Use this as a guide, keep in mind the following changes below: [http://www.shellhacks.com/en/SSH-with-Public-Key-Based-Authentication]()

       * press 'enter' key when prompted for filename, it will use the default name (id_rsa.pub)
       * passphrase is the password you would normally use to access the remote machine (ie. your trueffect domain password)
       * your 'scp' should look like this: scp -p id_rsa.pub YOU@SERVER:~/id_rsa.pub
           - example: scp -p id_rsa.pub wbutler@runamuck2:~/id_rsa.pub

   <a name="running-test-scripts"></a>

   ***

   ## Running Test Scripts
   There are two ways to run tests. You can run a script from the script folder directly, or you can run using Docker.  Please see the README.md file in the 'docker' folder.

   <a name="how-it-works"></a>

   ***

   ##How it Works

   ### Configuration Files
   Look in the 'config' folder of the project in IntelliJ.  Take a look at the "dev-ext_campaign_9173933.cfg" file.  The file has several configuration sections:

     * **[environment]** section: DEV and STG are defined here.  Comment out the one you don't want to use, and uncomment the env you want to use.  For this example, you can see it is using the DEV environment.  These values normally won't change, but commenting/uncommenting is necessary to use the environment you want to test against.
     * **[campaign]** section: This is where the campaign specific details go for both the campaign and the corresponding site measurement campaign.  Both need to be specified.
     * The rest of the sections normally do not ever need to be changes.  These are templates for each type of transaction (ie. I,C,P).  Take a quick look at the remaining sections, but don't worry too much about understanding every value.

   If there is a need to build a new campaign for testing, you'll need to create the corresponding configuration file in order for the automation to use the campaign data.

   ### Test Data
   Google Sheets is used to manage test data as well as test results.  Each SHEET corresponds to a different test.  Within a SHEET, each row corresponds to a single cookie log row.  Only the first 10 columns are used as input data columns.  The remaining columns are test results for each run.

   ### Special Scripts
   Inside the 'script' folder, the following scripts are used by the automation process and do not need to be modified:

     1. hive_count.sh - this script is generated by the automation process.  This is the hive query for the last run that counts the number of transactions in ods_stage for the run.  The process runs this script to determine if all the transactions have arrived in ods_stage yet.
     2. hive_get_failed_trans.sh - this script is generated by the automation process.  This is the hive query that determines which transactions failed.  It basically compares the expected bot flag with the actual bot flag and reports when they do not match.  The results of this query are saved to the Excel workbook.  When you run the Excel macro, it is using this data to highlight the transactions that failed.
     3. ip_cleanup.txt - a list if IPs that need to be cleaned up after each time the whitelist/blacklist test is run.  This makes these tests repeatable since the same IPs are used over and over.
     4. ip_cleanup-dev.sh - this script needs to be run after each time the whitelist/blacklist tests are run in DEV.
     5. ip_cleanup-stg.sh - this script needs to be run after each time the whitelist/blacklist tests are run in STG.
     6. ip_cleanup_data.txt - a list of BIDs and Transaction IDs to clean up.

   <a name="which-tests"></a>

   ***

   ## Which Tests do What?  How do I Run a Full Regression?
   See the README.md in the 'docker' folder.

   ***
